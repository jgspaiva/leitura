//
//  LivroViewCell.swift
//  Leitura
//
//  Created by Gustavo Paiva on 05/12/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

import UIKit

class LivroViewCell: UITableViewCell {
    @IBOutlet weak var capaImageView: UIImageView!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var autorLabel: UILabel!
    @IBOutlet weak var precoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
