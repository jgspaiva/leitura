//
//  LivrosTableViewController.swift
//  Leitura
//
//  Created by Gustavo Paiva on 05/12/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

import UIKit

class LivrosTableViewController: UITableViewController, APIProtocol, UISearchBarDelegate {
    let itunesAPI:ItunesAPI = ItunesAPI()
    var livrosData: NSDictionary? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        itunesAPI.delegate = self
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var qtLivros = 0
        
        if let dados = self.livrosData{
            qtLivros = dados["resultCount"] as! Int
        }
        
        return qtLivros
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "livroCell", for: indexPath) as! LivroViewCell
        
        if let dados = self.livrosData{
            let livroDictionary: NSDictionary = (dados["results"] as! NSArray)[indexPath.row] as! NSDictionary
            
            let livro:Livro = livroDictionary.toLivro()
            
            cell.tituloLabel!.text = livro.getTituloLivro()
            cell.autorLabel!.text = livro.getAutor()
            cell.precoLabel!.text = livro.getPreco()
            cell.capaImageView!.image = UIImage(data: NSData(contentsOf: NSURL(string: livro.getUrlCapa())! as URL)! as Data)
            
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        let detalhes = segue.destination as! DetalhesViewController
        
        if let dados = self.livrosData{
            let livro: Livro = ((dados["results"] as! NSArray)[self.tableView.indexPath(for: (sender as! LivroViewCell))!.row] as! NSDictionary).toLivro()
            
            detalhes.livro = livro
        }
    }
    
    
    // MARK: - SearchBar Delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        itunesAPI.searchItunesFor(searchTerm: searchBar.text!)
    }
    
    // MARK: - Itunes API
    
    func dadosRecebidos(dados: NSDictionary) {
        print("Recebidos \(dados["resultCount"] ?? 0) livros")
        
        DispatchQueue.global().async() {
            DispatchQueue.main.sync() {
                self.livrosData = dados
                self.tableView.reloadData()
            }
        }
    }

}
