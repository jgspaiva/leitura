//
//  DetalhesViewController.swift
//  Leitura
//
//  Created by Gustavo Paiva on 05/12/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

import UIKit
import CoreData

class DetalhesViewController: UIViewController {

    @IBOutlet weak var capaImageView: UIImageView!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var autorLabel: UILabel!
    @IBOutlet weak var precoLabel: UILabel!
    @IBOutlet weak var descricaoTextView: UITextView!
    
    var livro: Livro? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tituloLabel!.text = livro!.getTituloLivro()
        self.autorLabel!.text = livro!.getAutor()
        self.precoLabel!.text = livro!.getPreco()
        self.capaImageView!.image = UIImage(data: NSData(contentsOf: NSURL(string: livro!.getUrlCapa())! as URL)! as Data)
        
        var str: NSAttributedString?
        
        do{
            str = try NSAttributedString(data: livro!.getDescricao().data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
        }
        catch{
            str = NSAttributedString(string: "")
        }
        
        self.descricaoTextView.attributedText = str
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addFavorito(_ sender: Any) {
        
        /*let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let livroCD = NSEntityDescription.insertNewObject(forEntityName: "Aluno", into: context)
        
        do{
            try context.save()
        }
        catch{
            print("Erro ao salvar")
        }
        
        let consultaLivros = NSFetchRequest<NSFetchRequestResult>(entityName: "Livro")
        
        do {
            let livrosCD = try context.fetch(consultaLivros) as! [Any]
        }
        catch{
            print("Erro ao obter livros")
        }*/
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
