//
//  ItunesAPI.swift
//  Leitura
//
//  Created by Gustavo Paiva on 05/12/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

import Foundation

protocol APIProtocol{
    func dadosRecebidos(dados: NSDictionary)
}

class ItunesAPI : NSObject{
    var data:NSMutableData = NSMutableData()
    var delegate: APIProtocol?
    
    // Busca no iTunes
    func searchItunesFor(searchTerm: String) {
        
        let itunesSearchTerm = searchTerm.replacingOccurrences(of: " ", with: "+", options: .caseInsensitive, range: nil)
        
        let escapedSearchTerm = itunesSearchTerm.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.alphanumerics)!
        
        let url = URL(string: "https://itunes.apple.com/search?term=\(escapedSearchTerm)&media=ebook&limit=200")!
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                let jsonResult: NSDictionary = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                
                self.delegate?.dadosRecebidos(dados: jsonResult)
                
                
            } catch {
                print("json error: \(error)")
            }
        }
        task.resume()
    }
}
