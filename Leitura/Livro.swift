//
//  Livro.swift
//  Leitura
//
//  Created by Gustavo Paiva on 05/12/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

import Foundation

extension NSDictionary{
    func toLivro()->Livro{
        var saida: Livro?
        
        saida = Livro(tituloLivro: self["trackName"] as! String, urlCapa: self["artworkUrl100"] as! String, autor: self["artistName"] as! String, preco: self["formattedPrice"] as! String, descricao: self["description"] as! String)
        
        return saida!
    }
}

class Livro {
    private var tituloLivro:String = ""
    private var urlCapa: String = ""
    private var autor: String = ""
    private var preco: String = ""
    private var descricao: String = ""
    
    init(tituloLivro:String, urlCapa: String, autor: String, preco: String, descricao: String) {
        self.tituloLivro = tituloLivro
        self.urlCapa = urlCapa
        self.autor = autor
        self.preco = preco
        self.descricao = descricao
    }
    
    func getTituloLivro()->String{
        return self.tituloLivro
    }
    
    func getUrlCapa()->String{
        return self.urlCapa
    }
    
    func getAutor()->String{
        return self.autor
    }
    
    func getPreco()->String{
        return self.preco
    }
    
    func getDescricao()->String{
        return self.descricao
    }
}
